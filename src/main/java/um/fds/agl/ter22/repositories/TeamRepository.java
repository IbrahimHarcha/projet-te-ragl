package um.fds.agl.ter22.repositories;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Student;


import org.springframework.data.repository.CrudRepository;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.entities.Team;

public interface TeamRepository<T extends Team>  extends CrudRepository<T, Long> {

    @Override
    Team save(@Param("team")Team team);

    @Override
    void deleteById(@Param("id")Long id);

    @Override
    void delete(@Param("team")Team team);
}