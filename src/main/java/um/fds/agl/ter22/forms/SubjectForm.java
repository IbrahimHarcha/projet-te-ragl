package um.fds.agl.ter22.forms;

public class SubjectForm {
    private String title;
    private long id;
    private String teacher;

    private String supervisor;

    public SubjectForm(long id,String title,String teacher,String supervisor){
        this.id=id;
        this.title=title;
        this.teacher=teacher;
        this.supervisor=supervisor;
    }

    public SubjectForm(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
}
