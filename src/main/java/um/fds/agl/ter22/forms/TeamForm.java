package um.fds.agl.ter22.forms;

public class TeamForm {
    private String title;
    private long id;

    public TeamForm(long id,String title){
        this.id=id;
        this.title=title;
    }

    public TeamForm(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
