package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity

public class Subject {

    private @Id @GeneratedValue long id ;
    private String title;
    private String teacher;

    private String supervisor;

    public Subject(String title,String teacher, String supervisor){
        this.title=title;
        this.teacher=teacher;
        this.supervisor=supervisor;
    }

    public Subject(){};

    public long getId() {
        return id;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Subject)) return false;
        return(this.getTitle().equals(((Subject) o).getTitle()));
    }

    @Override
    public int hashCode(){
       int result=(int)this.getId();
       result=31*result+getTitle().hashCode();
       result=31*result+getTeacher().hashCode();
       return result;
    }
}
