package um.fds.agl.ter22.entities;

import javax.persistence.*;

@Entity
public class Team {
    @Id
    @GeneratedValue
    private Long id;

    public Team(){}
    public Team(String title){
        this.title=title;
    }

    private String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }


}
