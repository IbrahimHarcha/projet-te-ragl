package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.entities.Team;
import um.fds.agl.ter22.repositories.SubjectRepository;
import um.fds.agl.ter22.repositories.TeamRepository;

import java.util.Optional;

@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    public Optional<Team> getTeam(final Long id){return teamRepository.findById(id);}

    public Iterable<Team> getTeams(){return teamRepository.findAll();}

    public void deleteTeam(final Long id){teamRepository.deleteById(id);}

    public Team saveTeam(Team team){
        Team savedTeam=teamRepository.save(team);
        return savedTeam;
    }

    public Optional<Team> findById(long id){return teamRepository.findById(id);}
}
