package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.entities.Team;
import um.fds.agl.ter22.forms.SubjectForm;
import um.fds.agl.ter22.forms.TeamForm;
import um.fds.agl.ter22.services.SubjectService;
import um.fds.agl.ter22.services.TeamService;

@Controller
public class TeamController implements ErrorController {

    @Autowired
    private TeamService teamService;

    @GetMapping("/listTeams")
    public Iterable<Team> getTeams(Model model){
        model.addAttribute("teams",teamService.getTeams());
        return teamService.getTeams();
    }

    @GetMapping(value = {"/addTeam"})
    public String showAddTeamPage(Model model){
        TeamForm teamForm=new TeamForm();
        model.addAttribute("teamForm",teamForm);
        return "addTeam";

    }

    @PostMapping(value = {"/addTeam"})
    public String addTeam(Model model, @ModelAttribute("TeamForm") TeamForm teamForm){
        Team team;
        if(teamService.findById(teamForm.getId()).isPresent()){
            team=teamService.findById(teamForm.getId()).get();
            team.setTitle(teamForm.getTitle());
        }else{
            team=new Team(teamForm.getTitle());
        }
        teamService.saveTeam(team);
        return "redirect:/listTeams";
    }

    @GetMapping(value = {"/showTeamUpdateForm/{id}"})
    public String showTeamUpdateForm(Model model, @PathVariable(value = "id") long id){

        TeamForm teamForm = new TeamForm(id, teamService.findById(id).get().getTitle());
        model.addAttribute("teamForm", teamForm);
        return "updateTeam";
    }

    @GetMapping(value={"/deleteTeam/{id}"})
    public String deleteTeam(Model model, @PathVariable(value = "id")long id){
        teamService.deleteTeam(id);
        return "redirect:/listTeams";
    }
}