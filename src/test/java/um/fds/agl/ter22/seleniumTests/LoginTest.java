package um.fds.agl.ter22.seleniumTests;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginTest extends BaseForTests{

    @Test
    void successfulLogin()  {
        login("Chef", "mdp");
        assertTrue(driver.getTitle().contains("Page des TER de M1"));
    }

    @Test
    void passwordError(){
        login("Chef", "erreur");
        WebElement  error= driver.findElement(By.className("alert-danger"));
        String errorText=read(error);
        assertTrue(errorText.contains("Bad credentials"));
    }

    @Disabled
    @Test
    void teacherLogin(){
        login("monsieur","roucoups");
        assertTrue(driver.getTitle().contains("Page des TER de M1"));
    }

    @Test
    void teacherCannotAddTeacher(){
        goToAddTeacher("Turing","turing");
        assertTrue(driver.getTitle().contains("Error"));
    }

    @Test
    void teacherListDontChange(){
        login("Turing","turing");
        WebElement teacherListLink=driver.findElement(By.linkText("Teachers List"));
        click(teacherListLink);
        String exp=getTeacherList();
        WebElement addTeacherLink= driver.findElement(By.linkText("Add Teachers"));
        click((addTeacherLink));
        goBackToTeacherList();
        String actual=getTeacherList();
        assertEquals(exp,actual);

    }

    @Test
    void managerCanAddTeacher(){
        goToAddTeacher("Chef","mdp");
        WebElement firstNameField=driver.findElement(By.id("firstName"));
        WebElement lastNameField=driver.findElement(By.id("lastName"));
        WebElement createButton=driver.findElement(By.cssSelector("[type=submit]"));
        write(firstNameField,"FirstName");
        write(lastNameField,"LastName");
        click(createButton);
        assertTrue(driver.getTitle().contains("Teacher List"));
        assertTrue(getTeacherList().contains("FirstName"));
        assertTrue(getTeacherList().contains("LastName"));


    }

    // On note en passant que l'authentification, en cas de login erroné (ce serait un test à inclure ici),
    // affiche une vilaine exception ce qui n'est ni très sérieux ni très raisonnable,
    // il faudrait également renvoyer un bad credentials ...
}
